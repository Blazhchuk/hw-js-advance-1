/*

1.Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript {
    Прототипне насідування посилається на інші обєкти в обєкті, якщо шукати певну властивість якої немає в обєкті, вона буде шукатись в його прототипі, якщо такої там не буде - повернеться undefined
}

2.Для чого потрібно викликати super() у конструкторі класу-нащадка? {
    super() потрібно викликати для того щоб успадкувати функціональність від класу батька і також додати ще власні властивості 
}

*/

"use strict"

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(newName) {
        this._name = newName;
    }

    get age() {
        return this._age;
    }

    set age(newAge) {
        this._age = newAge;
    }

    get salary() {
        return this._salary;
    }

    set salary(newSalary) {
        this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get lang() {
        return this._lang;
    }

    set lang(newLang) {
        this._lang = newLang;
    }

    get salary() {
        return this._salary * 3;
    }

}

const prog1 = new Programmer('Anatoliy Sydorenko', 33, 15000, ['Python', 'JavaScript']);
const prog2 = new Programmer('Akakiy Serdelya', 28, 28000, ['C++', 'C#']);
const prog3 = new Programmer('Mykola Barvysty', 40, 50000, ['PHP', 'C', 'Java']);

console.log(`Programmer 1: Name: ${prog1.name}, Age: ${prog1.age}, Salary: ${prog1.salary}, Languages: ${prog1.lang.join(', ')}`);
console.log(`Programmer 2: Name: ${prog2.name}, Age: ${prog2.age}, Salary: ${prog2.salary}, Languages: ${prog2.lang.join(', ')}`);
console.log(`Programmer 3: Name: ${prog3.name}, Age: ${prog3.age}, Salary: ${prog3.salary}, Languages: ${prog3.lang.join(', ')}`);
